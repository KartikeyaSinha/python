---------------------------------------------------------------------------------------------------------------
**************************************************ARRAY qUESTIONS**********************************************
---------------------------------------------------------------------------------------------------------------
__________________________________
Sum & Average of the elements of a list
__________________________________

lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
sum=0
for i in range(0, n):
    sum=sum+lst[i]
avg=sum/n
print("Sum of the array is:", end="")
print(sum)
print("Average of the array is:" end="")
print(avg)

--------------------------------------------------------------------------------------------------------------------

____________________________________________
Median of a list (when elements are in ascending order)
____________________________________________


lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
if n%2==1:
    middle=n//2
    median=lst[middle]
    print("Median is :", median,end=" ")
elif n%2==0:
    middle=n//2
    median=(lst[middle]+lst[middle-1])/2
    print("Median is :", median,end=" ")
    

--------------------------------------------------
Median of a list (when elements are in random order) How to sort the array? without using python function
--------------------------------------------------

-----------------------------------------------------
MAX number in an array
-----------------------------------------------------
lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
max=lst[0]
for i in range(1,n):
    if lst[i]>max:
       max=lst[i]
    else:
       continue
print("The max number in the array is : ", end=" ")
print(max, end=" ")


--------------------------------------------------------
MIN number in an array
--------------------------------------------------------

lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
min=lst[0]
for i in range(1,n):
    if lst[i]<min:
       min=lst[i]
    else:
       continue
print("The min number in the array is : ", end=" ")
print(min, end=" ")

----------------------------------------------------------
Second Maximum number in an array
----------------------------------------------------------

lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
max=lst[0]
second_max=lst[1]
for i in range(1,n):
    if lst[i]>max:
       max=lst[i]
    elif lst[i]>second_max:
       second_max=lst[i]
print("The max number in the array is : ", end=" ")
print(max, end=" ")
print("The second max number in the array is : ", end=" ")
print(second_max, end=" ")

----------------------------------------------------------
Second Maximum number in an array
----------------------------------------------------------
lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
min=lst[0]
second_min=lst[1]
for i in range(1,n):
    if lst[i]<min:
       min=lst[i]
    elif lst[i]<second_min:
       second_min=lst[i]
print("The min number in the array is : ", end=" ")
print(min)
print("The second min number in the array is : ", end=" ")
print(second_min)

--------------------------------------------------------------------
RUNNING SUM IN AN ARRAY
------------------------------------------------------------------
lst = []
a = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
print(a)
sum=0
for i in range(n):
    sum=sum+lst[i]
    a.insert(i,sum)
print("Sum of the array is:", end="")
print(a)

--------------------------------------------------------------------
Shuffle Array //had to view the solution
------------------------------------------------------------------
lst = []
a = []
n = int(input("Enter number of elements : "))
x=n//2
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
for i in range(x):
    a.append(lst[i])
    a.append(lst[i+x])
print(a)

--------------------------------------------------------------------
Richest customer wealth //to be solved 2-d array
------------------------------------------------------------------



----------------------------------------------------------------------
Kids With the Greatest Number of Candies 
----------------------------------------------------------------------

candies = []
extraCandies=int(input("Enter extraCandies : "))
a=[]
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    candies.append(ele)       
print(candies)
maxcandies=candies[0]
for i in range(1,n):
    if candies[i]>maxcandies:
       maxcandies=candies[i]
    else:
       continue
print(maxcandies)
for i in range(n):
    if candies[i]+extraCandies >= maxcandies:
        a.append("True")
    else:
        a.append("False")
print(a)




----------------------------------------------------------------------
Number of good pairs
----------------------------------------------------------------------
lst = []
a = []
n = int(input("Enter number of elements : "))
x=n//2
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
count=0
        for i in range(len(lst)):
            for j in range(i+1,len(lst)):
                if lst[i] == lst[j]:
                    count+=1
        print(count)

----------------------------------------------------------------------
How many number are smaller than the current number
----------------------------------------------------------------------
lst = []
a = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
count=0
for i in range(len(lst)):
    count=0
    for j in range(len(lst)):
        if j!=i and lst[j] < lst[i]:
            count+=1
    a.append(count)
print(a)


----------------------------------------------------------------------
Deecompressed
----------------------------------------------------------------------
class Solution:
    def decompressRLElist(self, nums: List[int]) -> List[int]:
        a = []
        for i in range(1,len(nums)):
            if i%2 == 0:
                continue
            freq = nums[i-1]
            val = nums[i]
            for j in range(freq):
                a.append(val)
        return a

--------------------------------------------------------------------------
XOR Operation in an array
--------------------------------------------------------------------------
class Solution:
    def xorOperation(self, n: int, start: int) -> int:
        nums=[]
        for i in range(n):
            nums.append(start+2*i)
        x=nums[0]
        for i in range(1,n):
            x=x^nums[i]
        return x

----------------------------------------------------------------------------
CRAETE TARGET ARRAY IN THE GIVEN order
-----------------------------------------------------------------------------
class Solution:
    def createTargetArray(self, nums: List[int], index: List[int]) -> List[int]:
        target=[]
        for i in range(len(index)):
            for j in range(len(nums)):
                target.insert(index[i],nums[i])
                break
        return target     

----------------------------------------------------------------------------
2-D Array LOOp
-----------------------------------------------------------------------------
for i in range(len(accounts)):
            for j in range(len(accounts[0])):
                print(accounts[i][j], end=" ")
            print(" ")


------------------------------------------------------------------------------
Left Shift array elements by one
------------------------------------------------------------------------------
METHOD 2 (Rotate one by one)  

leftRotate(arr[], d, n)
start
  For i = 0 to i < d
    Left rotate all elements of arr[] by one
end
To rotate by one, store arr[0] in a temporary variable temp, move arr[1] to arr[0], arr[2] to arr[1] …and finally temp to arr[n-1]
Let us take the same example arr[] = [1, 2, 3, 4, 5, 6, 7], d = 2 
Rotate arr[] by one 2 times 
We get [2, 3, 4, 5, 6, 7, 1] after first rotation and [ 3, 4, 5, 6, 7, 1, 2] after second rotation.

lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
n=len(lst)
first=lst[0]
for i in range(n-1):
    lst[i]=lst[i+1]
lst[n-1]=first
print(lst)

-------------------------------------------------------------------------------------
RIGHT SHIFT ARRAY BY one
WHY WE HAVE TO TAKE ARRAY FROM N-1 TO 00 WHY NOT FROM 0 TO N-1?????
------------------------------------------------------------------------------------------

lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
n=len(lst)
last=lst[n-1]
for i in range(n-1,0,-1):
    lst[i]=lst[i-1]
lst[0]=last
print(lst)



--------------------------------------------------------------------------------
Two Sum
-----------------------------------------------------------------------------
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        a=[]
        for i in range(0, len(nums)):
            for j in range(i+1,len(nums)):
                if nums[i]+nums[j] == target:
                    a.append(i)
                    a.append(j)
        return a

--------------------------------------------------------------------------------------
Find the highest altitude/////Check the wrong answer: used my finding the maximum number program but it failed
--------------------------------------------------------------------------------------
class Solution:
    def largestAltitude(self, gain: List[int]) -> int:
        a=[]
        sum=0
        for i in range(len(gain)):
            sum=sum+gain[i]
            a.append(sum)
        a.insert(0,0)
        return max(a)


------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------
Given an array print all it unique elements ///not complete

-----------------------------------------------------------------------------------------------------------------
lst = []
n = int(input("Enter number of elements : "))
for i in range(0, n):
    ele = int(input())
    lst.append(ele)       
print(lst)
new=[]
new.append(lst[0])
for i in range(1,len(lst)):
    for j in range(len(new)):
        if lst[i]!=new[j]:
            new.append(lst[i])
    print(len(new))
print(new)

-----------------------------------------------------------------------------------------------------------------
Search Insert Position
--------------------------------------------------------------------------------------------------------------
class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        for i in range(len(nums)):
            if nums[i]>=target:
                return i
            elif target>nums[len(nums)-1]:
                return len(nums)
