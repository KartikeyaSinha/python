*
* *
* * *
* * * *
* * * * *

num=6
for i in range(1,num):
        i=i+1
        for j in range(1,i):
                print("*", end="")
        print("")

------------------------------------------------------------------------------------

1
1 2
1 2 3
1 2 3 4
1 2 3 4 5

num=6
for i in range(1,num):
        i=i+1
        for j in range(1,i):
                print(j, end="")
        print("")
--------------------------------------------------------------------------------------

A
A B
A B C
A B C D
A B C D E

num=70
for i in range(65,num):
        i=i+1
        for j in range(65,i):
                k = chr(j)
                print(k,end="")
        print("")
-------------------------------------------------------------------------------------------


A
B B
C C C
D D D D
E E E E E

num=69
for i in range(64,num):
        i=i+1
        for j in range(64,i):
                k = chr(i)
                print(k,end="")
        print("")

---------------------------------------------------------------------------------------------

* * * * *
* * * *
* * * 
* *
*
num=1
for i in range(6,num,-1):
        for j in range(1,i):
                print(*,end="")
        print("")
---------------------------------------------------------------------------------------------
1 2 3 4 5
1 2 3 4 
1 2 3
1 2
1

num=1
for i in range(6,num,-1):
        for j in range(1,i):
                print(j,end="")
        print("")
-----------------------------------------------------------------------------------------------
        *
      * * *
    * * * * *
  * * * * * * *
* * * * * * * * *

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(0,rows):
        z=rows-i
        y=(2*i)+1
        for j in range(0,z):
                print(" ", end="")
        for k in range(0,y):
                print("*",end="")
        print("")

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(0,rows):
        z=rows-i
        y=(2*i)+1
        j=" "
        k="*"
        print(z*j, end="")
        print(y*k,end="")
        print("")



-----------------------------------------------------------------------------------------------
1     
2 3
4 5 6
7 8 9 10


print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
number = 1
for i in range(1,rows+1):
        for j in range(1,i+1):
                print(number,end="")
                number=number+1
        print("")
------------------------------------------------------------------------------------------------
* * * * * * * * *
  * * * * * * *
    * * * * *
      * * *
        *
print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(0,rows):
        z=9-(2*i)
        for j in range(0,i):
                print(" ", end="")
        for k in range(0,z):
                print("*", end="")
        print("")

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(0,rows):
        j=" "
        k="*"
        z=9-(2*i)
        print(j*i, end="")
        print(z*k, end="")
        print("")

-------------------------------------------------------------------------------------------------
  
1
12
123
1234
12345

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
#number = 1
for i in range(1,rows+1):
        number = 1
        for j in range(1,i+1):
                print(j,end="")
                number=number+1
        print("")

-----------------------------------------------------------------------------------------------------------

123456
12345
1234
123
12
1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
k=rows+2
for i in range(1,rows+1):
        z = k-i
        for j in range(1,z):
                print(j,end="")
                z=z-1
        print("")

----------------------------------------------------------------------------------------------------------------
654321
65432
6543
654
65
6

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
        z=i-1
        for j in range(rows,z,-1):
                print(j,end="")
        print("")

-------------------------------------------------------------------------------------------------------------------
5
54
543
5432
54321

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
        z=rows-(i-1)
        for j in range(rows,z-1,-1):
                print(j,end="")
        print("")
--------------------------------------------------------------------------------------------------------------------


1
22
333
4444
55555

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
        z=i
        for j in range(1,z+1):
                print(z,end="")
        print("")

---------------------------------------------------------------------------------------------------------------------
55555
4444
333
22
1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(rows,0,-1):
        z=i
        for j in range(1,z+1):
                print(z,end="")
        print("")

------------------------------------------------------------------------------------------------------------
5
44
333
2222
11111

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
        for j in range(1,i+1):
                print(z,end="")
        z=z-1
        print("")

----------------------------------------------------------------------------------------------------------------
11111
2222
333
44
5

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=1
for i in range(rows,0,-1):
    for j in range(1,i+1):
        print(z,end="")
    z=z+1
    print("")

-----------------------------------------------------------------------------------------------------------------

12345
2345
345
45
5

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
    for j in range(i,rows+1):
        print(j,end="")
    print("")

----------------------------------------------------------------------------------------------------------------

12345
 2345
  345
   45
    5

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(i,rows+1):
        print(j,end="")
    print("")
-------------------------------------------------------------------------------------------------------------------

12345
 1234
  123
   12
    1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        print(j,end="")
    z=z-1
    print("")

----------------------------------------------------------------------------------------------------------------------

55555
 4444
  333
   22
    1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        print(z,end="")
    z=z-1
    print("")
----------------------------------------------------------------------------------------------------------

11111
 2222
  333
   44
    5


print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        print(i,end="")
    z=z-1
    print("")

--------------------------------------------------------------------------------------------------------------

     1
    12
   123
  1234
 12345

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(z,0,-1):
        print(" ",end="")
    for j in range(1,i+1):
        print(j,end="")
    z=z-1
    print("")

------------------------------------------------------------------------------------------------------------

     5
    54
   543
  5432
 54321

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(z,0,-1):
        print(" ",end="")
    for j in range(rows,z-1,-1):
        print(j,end="")
    z=z-1
    print("")

------------------------------------------------------------------------------------------------------------

     5
    45
   345
  2345
 12345

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows+1
kx=rows
for i in range(1,rows+1):
    for k in range(kx,0,-1):
        print(" ",end="")
    for j in range(z-i,rows+1):
        print(j,end="")
    kx=kx-1
    print("")

--------------------------------------------------------------------------------------------------------------
     1
    21
   321
  4321
 54321


print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows+1
kx=rows
for i in range(1,rows+1):
    for k in range(kx,0,-1):
        print(" ",end="")
    for j in range(i,0,-1):
        print(j,end="")
    kx=kx-1
    print("")
-------------------------------------------------------------------------------------------------------------
     1
    123
   12345
  1234567
 123456789

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(z,0,-1):
        print(" ",end="")
    x=(2*i)
    for j in range(1,i+1):
        print(j,end="")
    for l in range(i+1,x):
        print(l,end="")
    z=z-1
    print("")

---------------------------------------------------------------------------------------------------------------
     1
    121
   12321
  1234321
 123454321

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(z,0,-1):
        print(" ",end="")
    for j in range(1,i+1):
        print(j,end="")
    for l in range(i-1,0,-1):
        print(l,end="")
    z=z-1
    print("")

------------------------------------------------------------------------------------------------------------------

     5
    454
   34543
  2345432
 123454321

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows+1
kx=rows
for i in range(1,rows+1):
    for k in range(kx,0,-1):
        print(" ",end="")
    for j in range(z-i,rows+1):
        print(j,end="")
    kz=rows-(i-1)
    for l in range(rows-1,kz-1,-1):
        print(l,end="")
    kx=kx-1
    print("")

----------------------------------------------------------------------------------------------------------

123454321
 1234321
  12321
   121
    1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        print(j,end="")
    for l in range(rows-i,0,-1):
        print(l,end="")
    z=z-1
    print("")

----------------------------------------------------------------------------------------------------------

555555555
 4444444
  33333
   222
    1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        print(z,end="")
    for l in range(rows-i,0,-1):
        print(z,end="")
    z=z-1
    print("")

--------------------------------------------------------------------------------------------------------------

123454321
 2345432
  34543
   454
    5

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(i,rows+1):
        print(j,end="")
    for l in range(rows-1,i-1,-1):
        print(l,end="")
    print("")

----------------------------------------------------------------------------------------------------------------

1
10
101
1010
10101

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
     for j in range(1,i+1):
          if j%2==0:
               print("0", end="")
          else:
               print("1", end="")
     print("")

--------------------------------------------------------------------------------------------------------------------

0
01
010
0101
01010

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
     for j in range(1,i+1):
          if j%2==0:
               print("1", end="")
          else:
               print("0", end="")
     print("")

-------------------------------------------------------------------------------------------------------------------

1
00
111
0000
11111

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
     for j in range(1,i+1):
          if i%2==0:
               print("0", end="")
          else:
               print("1", end="")
     print("")

--------------------------------------------------------------------------------------------------------------------

1
01
101
0101
10101

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
for i in range(1,rows+1):
     for j in range(1,i+1):
          if i%2==0:
               if j%2==1:
                    print("0", end="")
               else:
                    print("1", end="")
          elif i%2==1:
               if j%2==1:
                    print("1", end="")
               else:
                    print("0", end="")
     print("")

--------------------------------------------------------------------------------------------------------------------

     0
    01
   010
  0101
 01010

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
     for k in range(z,0,-1):
          print(" ",end="")
     for j in range(1,i+1):
          if j%2==0:
               print("1", end="")
          else:
               print("0", end="")
     z=z-1
     print("")

--------------------------------------------------------------------------------------------------------------------

     1
    01
   101
  0101
 10101

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
     for k in range(z,0,-1):
          print(" ", end="")
     for j in range(1,i+1):
          if i%2==0:
               if j%2==1:
                    print("0", end="")
               else:
                    print("1", end="")
          elif i%2==1:
               if j%2==1:
                    print("1", end="")
               else:
                    print("0", end="")
     z=z-1
     print("")

----------------------------------------------------------------------------------------------------------------

10101
 1010
  101
   10
    1

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        if j%2==0:
               print("0", end="")
        else:
               print("1", end="")
    z=z-1
    print("")

---------------------------------------------------------------------------------------------------------------
01010
 1010
  010
   10
    0

print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        if i%2==0:
               if j%2==1:
                    print("1", end="")
               else:
                    print("0", end="")
        elif i%2==1:
               if j%2==1:
                    print("0", end="")
               else:
                    print("1", end="")
    z=z-1
    print("")

---------------------------------------------------------------------------------------------------------------------

00000
 1111
  000
   11
    0
print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        if i%2==0:
              print("1", end="")
        else:
              print("0", end="")
    z=z-1
    print("")

-------------------------------------------------------------------------------------------------------------------


010101010
 0101010
  01010
   010
    0


print("Enter number of rows of pyramid")
x_rows=input()
rows=int(x_rows)
z=rows
for i in range(1,rows+1):
    for k in range(1,i):
        print(" ",end="")
    for j in range(1,z+1):
        if j%2==0:
               print("1", end="")
        else:
               print("0", end="")
    for l in range(rows-i,0,-1):
        if l%2==0:
               print("1", end="")
        else:
               print("0", end="")
    z=z-1
    print("")

---------------------------------------------------------------------------------------------------------------



